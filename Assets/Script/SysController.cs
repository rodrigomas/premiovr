﻿using UnityEngine;
using System.Collections;
using System;

public class SysController : MonoBehaviour
{
    public GameObject MainCamera;

    public GameObject NextButton;
    public GameObject Dramaturgia;
    public GameObject Variedades;

    public GameObject Menu;

    public GameObject Parabens;

    public GameObject Scene;

    public AnimationManager SphereManager;

    public FrameHandler PicturesManager;

    public Vector3 OffSet = new Vector3(0, 0, 0.5f);

    private bool FadeIn = false;
    private bool FadeOut = false;
    private float FadeAlpha = 0.0f;
    private float FadeTimer = 0.0f;

    public float FadeMaxValue = 1000.0f;
    public float FadeDuration = 10.0f;
    public float FadePower = 2.0f;

    private bool InTransition = false;

    public enum Stages
    {
        Variedades,
        VariedadesFirst,
        VariedadesSecond,
        VariedadesThird,
        Dramaturgia,
        DramaturgiaFirst,
        DramaturgiaSecond,
        DramaturgiaThird,
        Ended
    };

    Stages stage = Stages.Dramaturgia;

    void Start ()
    {
        PicturesManager.OnFadeInEnded += PicturesManager_OnFadeInEnded;
        PicturesManager.OnFadeOutEnded += PicturesManager_OnFadeOutEnded;

        SphereManager.OnFadeInEnded += SphereManager_OnFadeInEnded;
        SphereManager.OnFadeOutEnded += SphereManager_OnFadeOutEnded;

        RunStage();
    }

    void OnFadeInEnded()
    {
        switch (stage)
        {
            case Stages.VariedadesThird:
                {
                    SphereManager.gameObject.SetActive(false);
                    SphereManager.SetFactor(1.0f);

                    PicturesManager.gameObject.SetActive(false);
                    Dramaturgia.SetActive(false);
                    Variedades.SetActive(false);
                    NextButton.SetActive(false);
                    Scene.SetActive(true);
                    Parabens.SetActive(true);
                    stage = Stages.Ended;

                    InTransition = false;
                }
                break;
        }
    }

    void OnFadeOutEnded()
    {
        switch (stage)
        {
            case Stages.Variedades:
                {
                    SphereManager.Set360(0);
                    SphereManager.SetFactor(1.0f);

                    Scene.SetActive(false);
                    SetFog(0);
                    PicturesManager.gameObject.SetActive(false);                    
                    Dramaturgia.SetActive(false);
                    Variedades.SetActive(false);

                    NextButton.SetActive(false);
                    SphereManager.gameObject.SetActive(true);

                    SphereManager.PlayFadeInAnimation();
                }
                break;        
        }
    }

    public void SetFog(float factor)
    {
        RenderSettings.fogDensity = factor * FadeMaxValue;
    }

    private void FadeApply()
    {
        RenderSettings.fogDensity = FadeAlpha * FadeMaxValue;
    }

    public void PlayFadeInAnimation()
    {
        FadeTimer = 0;
        FadeIn = true;
        FadeOut = false;
        FadeAlpha = 0;
    }

    public void PlayFadeOutAnimation()
    {
        FadeTimer = 0;
        FadeOut = true;
        FadeIn = false;
        FadeAlpha = 1.0f;
    }

    private void RunStage()
    {
        switch (stage)
        {
            case Stages.Dramaturgia:
                {
                    Parabens.SetActive(false);
                    Scene.SetActive(true);
                    SphereManager.gameObject.SetActive(false);
                    PicturesManager.gameObject.SetActive(false);
                    NextButton.SetActive(false);
                    Dramaturgia.SetActive(true);
                    Variedades.SetActive(false);
                }
                break;
            case Stages.Variedades:
                {
                    Parabens.SetActive(false);
                    Scene.SetActive(true);
                    SphereManager.gameObject.SetActive(false);
                    PicturesManager.gameObject.SetActive(false);
                    NextButton.SetActive(false);
                    Dramaturgia.SetActive(false);
                    Variedades.SetActive(true);
                }
                break;
        }
    }

    private void GoToNextStage()
    {
        if (InTransition)
            return;

        Debug.Log(stage);

        switch (stage)
        {
            case Stages.Variedades:
                {                    
                    PlayFadeOutAnimation();
                    Variedades.SetActive(false);
                    InTransition = true;
                }
                break;

            case Stages.VariedadesFirst:
            case Stages.VariedadesSecond:
            case Stages.VariedadesThird:
                {
                    NextButton.SetActive(false);
                    SphereManager.PlayFadeOutAnimation();
                    InTransition = true;
                }
                break;     

            case Stages.Dramaturgia:
                {
                    PicturesManager.gameObject.SetActive(true);
                    PicturesManager.SetAlpha(0.0f);
                    PicturesManager.SetPlayer(0);
                    Dramaturgia.SetActive(false);
                    InTransition = true;
                    PicturesManager.PlayFadeInAnimation();
                }
                break;

            case Stages.DramaturgiaFirst:
            case Stages.DramaturgiaSecond:
            case Stages.DramaturgiaThird:
                {
                    InTransition = true;
                    PicturesManager.PlayFadeOutAnimation();
                    NextButton.SetActive(false);
                }
                break;
        }
    }

    public void PressVariedades()
    {
        GoToNextStage();
    }

    public void PressDramaturgia()
    {
        GoToNextStage();
    }

    public void PressNext()
    {
        GoToNextStage();
    }

    private void SphereManager_OnFadeOutEnded()
    {
        switch (stage)
        {
            case Stages.VariedadesFirst:
                {
                    SphereManager.Set360(1);
                    SphereManager.PlayFadeInAnimation();
                }
                break;

            case Stages.VariedadesSecond:
                {
                    SphereManager.Set360(2);
                    SphereManager.PlayFadeInAnimation();
                }
                break;

            case Stages.VariedadesThird:
                {
                    SetFog(1.0f);                    
                    SphereManager.gameObject.SetActive(false);
                    PicturesManager.gameObject.SetActive(false);
                    NextButton.SetActive(false);                    
                    Variedades.SetActive(false);

                    Scene.SetActive(true);

                    PlayFadeInAnimation();
                }
                break;         
        }
    }

    private void SphereManager_OnFadeInEnded()
    {
        switch (stage)
        {
            case Stages.Variedades:
                {
                    stage = Stages.VariedadesFirst;
                    InTransition = false;
                    NextButton.SetActive(true);
                }
                break;

            case Stages.VariedadesFirst:
                {
                    stage = Stages.VariedadesSecond;
                    InTransition = false;
                    NextButton.SetActive(true);
                }
                break;
                
            case Stages.VariedadesSecond:
                {
                    stage = Stages.VariedadesThird;
                    InTransition = false;
                    NextButton.SetActive(true);///////////////////
                }
                break;                    
        }
    }

    private void PicturesManager_OnFadeOutEnded()
    {
        switch (stage)
        {
            case Stages.DramaturgiaFirst:
                {
                    PicturesManager.SetPlayer(1);
                    PicturesManager.PlayFadeInAnimation();
                }
                break;

            case Stages.DramaturgiaSecond:
                {
                    PicturesManager.SetPlayer(2);
                    PicturesManager.PlayFadeInAnimation();
                }
                break;

            case Stages.DramaturgiaThird:
                {
                    PicturesManager.gameObject.SetActive(false);
                    Parabens.SetActive(false);
                    NextButton.SetActive(false);
                    Dramaturgia.SetActive(false);
                    Variedades.SetActive(true);
                    InTransition = false;
                    stage = Stages.Variedades;
                }
                break;
        }
    }

    private void PicturesManager_OnFadeInEnded()
    {
        switch (stage)
        {
            case Stages.Dramaturgia:
                {
                    stage = Stages.DramaturgiaFirst;
                    InTransition = false;

                    NextButton.SetActive(true);
                }
                break;

            case Stages.DramaturgiaFirst:
                {
                    stage = Stages.DramaturgiaSecond;
                    InTransition = false;

                    NextButton.SetActive(true);
                }
                break;

            case Stages.DramaturgiaSecond:
                {
                    stage = Stages.DramaturgiaThird;
                    InTransition = false;

                    NextButton.SetActive(true);
                }
                break;

            case Stages.DramaturgiaThird:
                {
                }
                break;
        }
    }

    void OnDisable()
    {
        PicturesManager.OnFadeInEnded -= PicturesManager_OnFadeInEnded;
        PicturesManager.OnFadeOutEnded -= PicturesManager_OnFadeOutEnded;

        SphereManager.OnFadeInEnded -= SphereManager_OnFadeInEnded;
        SphereManager.OnFadeOutEnded -= SphereManager_OnFadeOutEnded;
    }    

    bool LeapFrog = false;

    void FixedUpdate()
    {
    }   

    void Update ()
    {

        LeapFrog = !LeapFrog;

        //if (LeapFrog)
        {
            if (Input.GetKeyUp(KeyCode.N))
            {
                GoToNextStage();
            }
        }

        Menu.transform.position = MainCamera.transform.position + OffSet;

        if (FadeIn)
        {
            FadeTimer += Time.deltaTime;

            FadeAlpha = Mathf.Pow(1.0f - FadeTimer / FadeDuration, FadePower);

            FadeApply();

            if (FadeTimer >= FadeDuration)
            {
                FadeIn = false;

                OnFadeInEnded();
            }
        }

        if (FadeOut)
        {
            FadeTimer += Time.deltaTime;

            FadeAlpha = Mathf.Pow(FadeTimer / FadeDuration, FadePower);

            FadeApply();

            if (FadeTimer >= FadeDuration)
            {
                FadeOut = false;

                OnFadeOutEnded();
            }
        }
    }
}
