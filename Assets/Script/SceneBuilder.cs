﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class SceneBuilder : MonoBehaviour
{
    public GameObject [] RedPrefab = null;
    public GameObject[] BlackPrefab = null;

    public Wave[] Waves = null;

    public Camera CenterObject = null;

    public int Width = 10;
    public int Height = 10;
    public int Length = 10;

    public Vector3 CubeScale = Vector3.one;

    [Range(0.0f, 1.0f)]
    public float FloorRoofBlackProbability = 0.7f;

    [Range(0.0f, 1.0f)]
    public float WallBlackProbability = 0.7f;

    [Range(0.0f, 1.0f)]
    public float NoiseStrength = 0.2f;

    private GameObject[,,] Matrix = null;

    private List<int> ValidX;
    private List<int> ValidY;
    private List<int> ValidZ;

    private UnityStandardAssets.ImageEffects.BloomOptimized Bloom = null;

    private Vector3 TempPos = new Vector3();
    private Vector3 P0 = new Vector3();

    // Use this for initialization
    void Start ()
    {
        Bloom = CenterObject.gameObject.GetComponent<UnityStandardAssets.ImageEffects.BloomOptimized>();

        GameObject[][] Vectors = new GameObject[][] { RedPrefab, BlackPrefab, BlackPrefab, RedPrefab };

        Matrix = new GameObject[Width, Height, Length];

        ValidX = new List<int>(new int[] { 0, 1, Width - 2, Width - 1 });
        ValidZ = new List<int>(new int[] { 0, 1, Length - 2, Length - 1 });
        ValidY = new List<int>(new int[] { 0, 1, Height - 2, Height - 1 });

        for (int i = 0; i < Width; i++)
        {           
            for (int k = 0; k < Length; k++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (Matrix[i, j, k] != null)
                        return;

                    Matrix[i, j, k] = null;

                    if ( ValidY.Contains(j) )
                    {
                        int vID = ValidY.FindIndex(x => x == j);

                        if (Random.value < FloorRoofBlackProbability || Vectors[vID] != BlackPrefab)
                        {
                            CreateBox(Vectors, i, k, j, vID);

                        }
                    } else if ( ValidX.Contains(i) )
                    {
                        int vID = ValidX.FindIndex(x => x == i);

                        if (Random.value < WallBlackProbability || Vectors[vID] != BlackPrefab)
                        {
                            CreateBox(Vectors, i, k, j, vID);

                        }
                    }
                    else if (ValidZ.Contains(k))
                    {
                        int vID = ValidZ.FindIndex(x => x == k);

                        if (Random.value < WallBlackProbability || Vectors[vID] != BlackPrefab)
                        {
                            CreateBox(Vectors, i, k, j, vID);

                        }
                    }
                }
            }
        }
    }

    void OnEnable()
    {
        if(Bloom != null)
            Bloom.enabled = true;
    }

    private void CreateBox(GameObject[][] Vectors, int i, int k, int j, int vID)
    {
        Matrix[i, j, k] = Instantiate<GameObject>(Vectors[vID][Random.Range(0, Vectors[vID].Length)]);
        Matrix[i, j, k].transform.SetParent(gameObject.transform);
        Matrix[i, j, k].name = string.Format("Block_Tile_{0}_{1}_{2}", i, j, k);

        Matrix[i, j, k].transform.Translate((i - Width / 2) * CubeScale.x, (j - Height / 2) * CubeScale.y, (k - Length / 2) * CubeScale.z);
        Matrix[i, j, k].transform.localScale = CubeScale;
    }

    void OnDisable()
    {
        if (Bloom != null)
            Bloom.enabled = false;        
    }

    void OnDestroy()
    {
        if (Matrix != null)
        {
            for (int i = 0; i < Width; i++)
            {
                for (int k = 0; k < Length; k++)
                {
                    for (int j = 0; j < Height; j++)
                    {
                        if (Matrix[i, j, k] != null)
                        {
                            Destroy(Matrix[i, j, k]);
                            Matrix[i, j, k] = null;
                        }
                    }
                }
            }
        }

        Matrix = null;
    }

    void Update()
    {
        gameObject.transform.position = CenterObject.transform.position;

        for (int i = 0; i < Width; i++)
        {
            for (int k = 0; k < Length; k++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (Matrix[i, j, k] != null)
                    {
                        P0.x = Matrix[i, j, k].transform.position.x;
                        P0.y = Matrix[i, j, k].transform.position.y;
                        P0.z = Matrix[i, j, k].transform.position.z;

                        if (ValidY.Contains(j))
                        {
                            TempPos.x = Matrix[i, j, k].transform.position.x;
                            TempPos.y = 0;
                            TempPos.z = Matrix[i, j, k].transform.position.z;

                            float H = 0;
                            H = ComputeHeight(H);

                            Matrix[i, j, k].transform.position = new Vector3(P0.x, (H + j - Height / 2) * CubeScale. y, P0.z);
                        }
                        else if (ValidX.Contains(i))
                        {

                            TempPos.x = Matrix[i, j, k].transform.position.z;
                            TempPos.y = 0;
                            TempPos.z = Matrix[i, j, k].transform.position.y;

                            float H = 0;
                            H = ComputeHeight(H);

                            Matrix[i, j, k].transform.position = new Vector3( (H + i - Width / 2) * CubeScale. x, P0.y, P0.z);
                        }
                        else if (ValidZ.Contains(k))
                        {

                            TempPos.x = Matrix[i, j, k].transform.position.x;
                            TempPos.y = 0;
                            TempPos.z = Matrix[i, j, k].transform.position.y;

                            float H = 0;
                            H = ComputeHeight(H);

                            Matrix[i, j, k].transform.position = new Vector3(P0.x, P0.y, (H + k - Length / 2) * CubeScale.z);
                        }
                    }
                }
            }
        }
    }

    private float ComputeHeight(float H)
    {
        for (int l = 0; l < Waves.Length; l++)
        {
            float W = 2.0f * Mathf.PI / Waves[l].Wavelength;

            H += Waves[l].Amplitude * Mathf.Sin(Vector3.Dot(Waves[l].Direction, TempPos) * W + Waves[l].Speed * W * Time.time);
        }

        return H;
    }
}
