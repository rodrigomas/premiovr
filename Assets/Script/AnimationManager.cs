﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour
{
    public Material material;
    public Cubemap[] Cubes;

    private bool FadeIn = false;
    private bool FadeOut = false;
    private float FadeAlpha = 0.0f;
    private float FadeTimer = 0.0f;

    public float FadeDuration = 10.0f;
    public float FadePower = 2.0f;

    public delegate void FRAnimationEndedDelegate();

    public event FRAnimationEndedDelegate OnFadeInEnded = null;

    public event FRAnimationEndedDelegate OnFadeOutEnded = null;

    // Use this for initialization
    void Start()
    {
        material.SetTexture("_Cube", Cubes[0]);
    }

    // Update is called once per frame
    void Update()
    {
        if (FadeIn)
        {
            FadeTimer += Time.deltaTime;

            FadeAlpha = Mathf.Pow(1.0f - FadeTimer / FadeDuration, FadePower);

            FadeApply();

            if (FadeTimer >= FadeDuration)
            {
                FadeIn = false;

                if (OnFadeInEnded != null)
                    OnFadeInEnded();
            }
        }

        if (FadeOut)
        {
            FadeTimer += Time.deltaTime;

            FadeAlpha = Mathf.Pow(FadeTimer / FadeDuration, FadePower);

            FadeApply();

            if (FadeTimer >= FadeDuration)
            {
                FadeOut = false;

                if (OnFadeOutEnded != null)
                    OnFadeOutEnded();
            }
        }
    }

    public void SetFactor(float factor)
    {
        material.SetFloat("_Factor", factor);
    }

    private void FadeApply()
    {
        material.SetFloat("_Factor", FadeAlpha);
    }

    public void PlayFadeInAnimation()
    {
        FadeTimer = 0;
        FadeIn = true;
        FadeOut = false;
        FadeAlpha = 0;        
    }

    public void PlayFadeOutAnimation()
    {
        FadeTimer = 0;
        FadeOut = true;
        FadeIn = false;
        FadeAlpha = 1.0f;
    }

    public void Set360(int i)
    {
        if(Cubes != null && i >= 0 && i < Cubes.Length)
            material.SetTexture("_Cube", Cubes[i]);
    }
}
