﻿using UnityEngine;
using System.Collections;

public class Anima3DManager : MonoBehaviour {


    public GameObject [] Models;

    public GameObject Menu;
    public GameObject Winner;
    public GameObject Variedades;
    public Vector3 OffSet = new Vector3(0, 0, 0.5f);

    public Animator animator;


    // Use this for initialization
    void Start()
    {
        Winner.SetActive(false);
        Variedades.SetActive(true);
        SetVisible(0);
    }

    void SetVisible(int x)
    {
        for(int i = 0; i< Models.Length; i++)
        {
            Models[i].SetActive(i == x);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Menu.transform.position = Camera.main.transform.position + OffSet;
    }

    public void StopAll()
    {
        animator.StopPlayback();
    }

    public void PlayWinner()
    {
        Winner.SetActive(false);
        Variedades.SetActive(false);
        animator.SetTrigger("PlayWinner");
    }

    public void PlayAll()
    {
        Variedades.SetActive(false);
        animator.SetTrigger("Play");
    }

    public void GetBack()
    {
        SetVisible(0);

        animator.SetTrigger("Back");
        Variedades.SetActive(false);
        Winner.SetActive(true);
    }

    public void SetModel(int i)
    {
        SetVisible(i);
    }
}
