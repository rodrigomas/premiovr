﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ReflectionProbe))]
public class ReflectionUpdate : MonoBehaviour {

    ReflectionProbe probe = null;
	
	void Start ()
    {
        probe = GetComponent<ReflectionProbe>();
	}
	
	
	void FixedUpdate ()
    {
        probe.RenderProbe();
    }
}
