﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Wave
{
    public float Wavelength = 5f;
    public float Amplitude = 0.05f;
    public float Speed = 0.01f;
    public Vector3 Direction = Vector3.forward;
}