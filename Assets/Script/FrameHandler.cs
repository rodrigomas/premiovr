﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FrameHandler : MonoBehaviour
{

    public GameObject BaseFrame = null;

    [Range(0.0f, 10.0f)]
    public float InnerRadius = 3.0f;

    [Range(0.0f, 10.0f)]
    public float OuterRadius = 3.5f;

    [Range(0.0f, 360.0f)]
    public float AvoidanceAngle = 30f;

    [Range(-180.0f, 180.0f)]
    public float AvoidancePhase = 0;

    [Range(0.0f, 10.0f)]
    public float Height = 2.0f;

    [Range(-10.0f, 10.0f)]
    public float HeightOffset = 1.0f;

    [Range(-10.0f, 10.0f)]
    public float CenterHeightOffset = 1.0f;

    [Range(0.0f, 1.0f)]
    public float Alpha = 1.0f;

    [Range(0.0f, 4.0f)]
    public float AspectRatio = 1.33f;

    [Range(0.0f, 10.0f)]
    public float MainScale = 1.5f;

    [Range(1, 10)]
    public int HeightSteps = 3;

    [Range(1, 10)]
    public int DepthSteps = 3;

    public float DistanceScaleFactor = 2.0f;

    public GameObject MainCamera;

    public Texture[] Player01 = null;
    public Texture[] Player02 = null;
    public Texture[] Player03 = null;

    public float[] Player01Aspect = null;
    public float[] Player02Aspect = null;
    public float[] Player03Aspect = null;

    private bool FadeIn = false;
    private bool FadeOut = false;
    private float FadeAlpha = 0.0f;
    private float FadeTimer = 0.0f;

    public float FadeDuration = 10.0f;
    public float FadePower = 2.0f;

    public float Wavelength = 5.0f;
    public float Amplitude = 0.3f;

    public Material BaseMaterial = null;

    private Material[] Materials = null;

    private GameObject[] Objects = null;

    private Vector2[] Phase = null;
    private float [] Scales = null;

    private int MaxAlloc = 0;

    public delegate void FRAnimationEndedDelegate();

    public event FRAnimationEndedDelegate OnFadeInEnded = null;

    public event FRAnimationEndedDelegate OnFadeOutEnded = null;

    void Start()
    {
        //Builded = false;

        //Build();

        //SetPlayer(0);
    }

    private bool Builded = false;

    void OnEnable()
    {
        if (!Builded)
            Build();
    }

    private void Build()
    {
        if (Builded)
            return;

        MaxAlloc = Mathf.Max(Player01.Length, Player02.Length, Player03.Length);

        Scales = new float[MaxAlloc];
        Materials = new Material[MaxAlloc];

        for (int i = 0; i < Materials.Length; i++)
        {
            Materials[i] = Instantiate<Material>(BaseMaterial);
        }

        float AngleStep = (360.0f - AvoidanceAngle) / MaxAlloc;

        float YStep = Height / HeightSteps;

        float DepthStep = (OuterRadius - InnerRadius) / DepthSteps;

        float[] YSteps = new float[HeightSteps];

        for (int i = -HeightSteps / 2, j = 0; j < YSteps.Length; i++, j++)
        {
            YSteps[j] = YStep * i;
        }

        float[] DSteps = new float[DepthSteps];

        for (int i = -DepthSteps / 2, j = 0; j < DSteps.Length; i++, j++)
        {
            DSteps[j] = DepthStep * i;
        }

        Objects = new GameObject[MaxAlloc];
        Phase = new Vector2[MaxAlloc];
        int LastH = 0;
        int LastD = 0;
        for (int i = 0; i < Objects.Length; i++)
        {
            Scales[i] = 1.0f;
            Objects[i] = Instantiate<GameObject>(BaseFrame);

            Objects[i].transform.SetParent(gameObject.transform);
            Objects[i].name = string.Format("Frame_Tile_{0}", i);

            Renderer renderer = Objects[i].GetComponent<Renderer>();
            renderer.material = Materials[i];

            Vector3 P = new Vector3();

            if (i == 0)
            {
                Phase[i] = Vector2.zero;

                Vector2 posXZ = new Vector2(0, 1);

                posXZ = posXZ * InnerRadius;

                float H = CenterHeightOffset + MainCamera.transform.position.y;

                P.x = posXZ.x; P.y = H; P.z = posXZ.y;

                Objects[i].transform.forward = new Vector3(-P.x, 0, -P.z);

            }
            else
            {
                Phase[i].x = Random.value * 2 * Mathf.PI;

                //float angle = AvoidancePhase + AngleStep * (i);//Random.Range(AvoidanceAngle / 2.0f, 360.0f - AvoidanceAngle / 2.0f);
                float angle = AngleStep * (i + 1);//Random.Range(AvoidanceAngle / 2.0f, 360.0f - AvoidanceAngle / 2.0f);

                angle = angle * Mathf.PI / 180.0f;

                Vector2 posXZ = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));

                int pH = Random.Range(0, HeightSteps);

                if (pH == LastH) pH = ((pH + 1) % HeightSteps);

                LastH = pH;

                float H = HeightOffset + MainCamera.transform.position.y + YSteps[pH]; //(Random.value * 2.0f - 1.0f) * Height;                                

                int pD = Random.Range(0, DepthSteps);

                if (pD == LastD) pD = ((pD + 1) % DepthSteps);

                LastD = pD;

                posXZ = posXZ * (InnerRadius + (OuterRadius - InnerRadius) * DSteps[pD]);

                float scaleFator = ((float)pD) / DSteps.Length;

                P.x = posXZ.x; P.y = H; P.z = posXZ.y;

                Scales[i] = 1.0f + scaleFator * DistanceScaleFactor;

                Phase[i].y = H;



                Objects[i].transform.forward = new Vector3(-P.x, -P.y, -P.z);

                Objects[i].transform.localScale = new Vector3(scaleFator, scaleFator, Objects[i].transform.localScale.z);
            }

            Objects[i].transform.position = P;
        }

        SetPlayer(0);

        Builded = true;
    }

    public void PlayFadeInAnimation()
    {
        FadeTimer = 0;
        FadeIn = true;
        FadeOut = false;
        FadeAlpha = 0;
    }

    public void PlayFadeOutAnimation()
    {
        FadeTimer = 0;
        FadeOut = true;
        FadeIn = false;
        FadeAlpha = 1.0f;
    }

    void FixedUpdate()
    {
        for (int i = 1; i < MaxAlloc; i++)
        {
            if (!Objects[i].activeSelf)
                continue;

            float H = Phase[i].y + Amplitude * Mathf.Sin(Phase[i].x + Time.fixedTime * 2.0f * Mathf.PI / Wavelength);

            Objects[i].transform.position = new Vector3(Objects[i].transform.position.x, H, Objects[i].transform.position.z);
        }
    }

    void Update()
    {
        if (FadeIn)
        {
            FadeTimer += Time.deltaTime;

            FadeAlpha = Mathf.Pow(FadeTimer / FadeDuration, FadePower);

            FadeApply();

            if (FadeTimer >= FadeDuration)
            {
                FadeIn = false;

                if (OnFadeInEnded != null)
                    OnFadeInEnded();
            }
        }

        if (FadeOut)
        {
            FadeTimer += Time.deltaTime;

            FadeAlpha = Mathf.Pow(1.0f - FadeTimer / FadeDuration, FadePower);

            FadeApply();

            if (FadeTimer >= FadeDuration)
            {
                FadeOut = false;

                if (OnFadeOutEnded != null)
                    OnFadeOutEnded();
            }
        }
    }
    
    private void FadeApply()
    {
        for (int i = 0; i < MaxAlloc; i++)
        {
            if (!Objects[i].activeSelf)
                continue;

            Renderer renderer = Objects[i].GetComponent<Renderer>();
            renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, FadeAlpha);

            //renderer.material.SetColor("_Color", new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, FadeAlpha));
        }
    }

    public void SetAlpha(float alpha)
    {
        for (int i = 0; i < MaxAlloc; i++)
        {
            if (!Objects[i].activeSelf)
                continue;

            Renderer renderer = Objects[i].GetComponent<Renderer>();
            renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, alpha);

            //renderer.material.SetColor("_Color", new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, FadeAlpha));
        }
    }

    public void PlayNormalAnimation()
    {
        for (int i = 1; i < MaxAlloc; i++)
        {
            Renderer renderer = Objects[i].GetComponent<Renderer>();
            renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, 1.0f);

           // renderer.material.SetColor("_Color", new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, FadeAlpha));
        }
    }

    public void SetPlayer(int id)
    {
        Texture[] Buffer = GetTexBuffer(id);

        float[] aspects = GetAspectBuffer(id);

        int n = Buffer.Length;

        List<int> Ids = new List<int>();

        for (int i = 1; i < MaxAlloc; i++)
        {
            Objects[i].SetActive(false);
        }

        for (int i = 0; i <= MaxAlloc / 2; i++)
        {
            Ids.Add(i);

            Ids.Add(MaxAlloc - i - 1);
        }   
            

        for (int i = 1; i < n; i++)
        {
            int idx = Ids[i];//Random.Range(0, Ids.Count);

            Objects[idx].SetActive(true);

            float aspect = (float)Buffer[i].width / (float)Buffer[i].height;

            aspect = (aspects != null && idx < aspects.Length) ? aspects[idx] : aspect;

            Objects[idx].transform.localScale = new Vector3(aspect * Scales[idx], 1.0f * Scales[idx], Objects[idx].transform.localScale.z);

            Renderer renderer = Objects[idx].GetComponent<Renderer>();
            renderer.material.SetTexture("_DetailAlbedoMap", Buffer[i]);

            //if (i != 0) Ids.Remove(idx);
        }

        float mainAspect = ((float)Buffer[0].width / (float)Buffer[0].height);

        mainAspect = (aspects != null && 0 < aspects.Length) ? aspects[0] : mainAspect;

        Objects[0].SetActive(true);
        Renderer mainRenderer = Objects[0].GetComponent<Renderer>();
        mainRenderer.material.SetTexture("_DetailAlbedoMap", Buffer[0]);
        Objects[0].transform.localScale = new Vector3(mainAspect * MainScale * Scales[0], 1.0f * MainScale * Scales[0], Objects[0].transform.localScale.z);
    }

    private Texture[] GetTexBuffer(int id)
    {
        return (id == 0) ? Player01 : ((id == 1) ? Player02 : Player03);
    }

    private float[] GetAspectBuffer(int id)
    {
        return (id == 0) ? Player01Aspect : ((id == 1) ? Player02Aspect : Player03Aspect);
    }

    void OnDestroy()
    {
        if (Materials != null)
        {
            for (int i = 0; i < Materials.Length; i++)
            {
                Destroy(Materials[i]);
            }

            Materials = null;
        }

        if (Objects != null)
        {
            for (int i = 0; i < Objects.Length; i++)
            {
                Destroy(Objects[i]);
            }

            Objects = null;
        }
    }
}
