﻿Shader "Unlit/SphereBlend"
{
	Properties
	{
		_Cube01("Cubemap 01", CUBE) = "" {}
		_Cube02("Cubemap 02", CUBE) = "" {}
		_Cube03("Cubemap 03", CUBE) = "" {}

		_Factor("Factor", Range(0,2)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Cull off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};

			struct v2f
			{
				float3 worldPos : TEXCOORD1;
				float3 worldNormal : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			samplerCUBE _Cube01;
			samplerCUBE _Cube02;
			samplerCUBE _Cube03;

			half _Factor;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				o.worldNormal = -UnityObjectToWorldNormal(v.normal);

				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));

				float3 worldRefl = reflect(worldViewDir, i.worldNormal);
				// sample the texture
				fixed4 col0 = texCUBE(_Cube01, worldRefl);
				fixed4 col1 = texCUBE(_Cube02, worldRefl);
				fixed4 col2 = texCUBE(_Cube03, worldRefl);

				float factor0 = (_Factor > 1.0f) ? 0.0f : 1.0f - _Factor;
				float factor1 = (_Factor > 1.0f) ? _Factor - 1.0f : _Factor;
				float factor2 = (_Factor > 1.0f) ?  2.0f - _Factor : 0.0f;

				fixed4 col = col0 * factor0 + col1 * factor1 + col2 * factor2;
				// apply fog				
				return col;
			}
			ENDCG
		}
	}
}
