﻿Shader "Custom/Illumination" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DetailTex("Detail Mask", 2D) = "white" {}
		_DetailAlbedoMap("Detail Albedo", 2D) = "white" {}
		_Intensity ("Intensity", Range(0,10)) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _DetailAlbedoMap;
		sampler2D _DetailTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Intensity;

		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 mask = tex2D(_DetailTex, IN.uv_MainTex);
			fixed4 cDetail = tex2D(_DetailAlbedoMap, IN.uv_MainTex);
			o.Albedo = c.rgb * mask.rgb * cDetail.rgb * _Intensity;
			o.Alpha = _Color.a;

			o.Metallic = 0;
			//o.Emission = _Color;
			o.Smoothness = 0;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
