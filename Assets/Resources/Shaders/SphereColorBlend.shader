﻿Shader "Unlit/SphereColorBlend"
{
	Properties
	{
		_Cube("Cubemap", CUBE) = "" {}		
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Factor("Factor", Range(0,1)) = 1
		_Power("Power", Range(0,10)) = 2
	}
		SubShader
		{
			Tags{ "RenderType" = "Opaque" }
			LOD 100
			Cull off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag						

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal: NORMAL;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float2 texcoord : TEXCOORD2;
				float3 worldPos : TEXCOORD1;
				float3 worldNormal : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			samplerCUBE _Cube;
			sampler2D _MainTex;

			half _Factor;
			half _Power;
			float4 _MainTex_ST; 

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				o.worldNormal = -UnityObjectToWorldNormal(v.normal);

				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float4 color = tex2D(_MainTex, i.texcoord);
				color.a = 1;

				float3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));

				float3 worldRefl = reflect(worldViewDir, i.worldNormal);

				float factor = pow(_Factor, _Power);

				fixed4 col = texCUBE(_Cube, worldRefl) *  (1.0f - factor) + factor * color;
				
				return col;
			}
		ENDCG
	}
	}
}
